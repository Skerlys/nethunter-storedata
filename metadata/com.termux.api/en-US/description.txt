The Termux:API add-on provides command line access to device API:s:

* Read and send sms messages from your terminal.
* Access device GPS location sensor from scripts.
* Pipe the result of commands into the device text-to-speech engine.
* Vibrate the device when something interesting happens.
* Access the system clipboard from shell scripts.
* List contacts from the system contact list.

Besides installing this app an additional package is required to install inside Termux:
$ apt install termux-api

See the following documentation about available API commands:
https://wiki.termux.com/wiki/Termux:API

NOTE: This is an add-on which requires that the main Termux app is installed to use.