Rucky is a modern looking USB Rubber Ducky Editor and Attack Launcher.

Features
    - Rubber Ducky Script Editor
    - Rubber Ducky Script Launcher
    - Dark Theme
    - Architecture feature binary support
    - Shealth mode. Hide it from the launcher. Launch the app from dialer.

Requires root and a kernel with USB HID patch applied.