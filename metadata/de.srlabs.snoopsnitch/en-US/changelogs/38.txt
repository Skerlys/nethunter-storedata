* Fixed autostarting mobile network security test on boot
* Fixed notifications on Android OS versions >= 8.0
* Minor bugfix
